from typing import Optional

from pydantic import *


class MyModel(BaseModel):
    inventory_quantity: int
    inventory_policy: str

    @computed_field
    @property
    def effective_quantity(self) -> Optional[int]:
        if self.inventory_policy == "deny":
            return self.inventory_quantity
        return None


for model in [
    MyModel(inventory_quantity=0, inventory_policy="deny"),
    MyModel(inventory_quantity=10, inventory_policy="continue"),
]:
    print(model.model_dump_json(indent=2))


